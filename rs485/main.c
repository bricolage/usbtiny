// ======================================================================
// Control a parallel port AVR programmer (avrdude type "bsd") via USB.
//
// Copyright (C) 2006 Dick Streefland
//
// This is free software, licensed under the terms of the GNU General
// Public License as published by the Free Software Foundation.
// ======================================================================

#define F_CPU 12000000L
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "usb.h"

#ifdef RS485_SUPPORT
#include "fifo.h"
#endif

enum
{
	// Generic requests
	USBTINY_ECHO,		// echo test
	USBTINY_READ,		// read byte (wIndex:address)
	USBTINY_WRITE,		// write byte (wIndex:address, wValue:value)
	USBTINY_CLR,		// clear bit (wIndex:address, wValue:bitno)
	USBTINY_SET,		// set bit (wIndex:address, wValue:bitno)
	// Programming requests
	USBTINY_POWERUP,	// apply power (wValue:SCK-period, wIndex:RESET)
	USBTINY_POWERDOWN,	// remove power from chip
	USBTINY_SPI,		// issue SPI command (wValue:c1c0, wIndex:c3c2)
	USBTINY_POLL_BYTES,	// set poll bytes for write (wValue:p1p2)
	USBTINY_FLASH_READ,	// read flash (wIndex:address)
	USBTINY_FLASH_WRITE,	// write flash (wIndex:address, wValue:timeout)
	USBTINY_EEPROM_READ,	// read eeprom (wIndex:address)
	USBTINY_EEPROM_WRITE,	// write eeprom (wIndex:address, wValue:timeout)
#ifdef I2C_SUPPORT
	USBI2C_READ = 20,
	USBI2C_WRITE,
	USBI2C_STOP,
	USBI2C_STAT,
#endif
#ifdef RS485_SUPPORT
        USBRS485_SETUP = 30,   // setup frame format, and baud rate
        USBRS485_START_TX,     // enable transmitter
        USBRS485_STOP_TX,     // disable transmitter
        USBRS485_WRITE,        // write data out to the rs485 bus without 9th bit
        USBRS485_READ_BUFFER_LEN,        // Read the len of the recieve buffer
        USBRS485_READ_BUFFER,        // Read the the recieve buffer
        USBRS485_CLEAR_BUFFER        // Read the the recieve buffer
#endif
};

#ifdef I2C_SUPPORT
enum
{
	I2C_PAKET		= 0x01,
	I2C_READ		= 0x02,
	I2C_READ_ON	        = 0x04,
	I2C_WRITE		= 0x08,
	I2C_STAT		= 0x10,
};
#endif

#ifdef RS485_SUPPORT
enum Rs485State
{ 
  RS485_SETUP = 1,
  RS485_WRITE,
  RS485_STATUS,
  RS485_READ,
};
#endif

enum ModuleMode
{
  MODE_SPI,
#ifdef I2C_SUPPORT
  MODE_I2C,
#endif
#ifdef RS485_SUPPORT
  MODE_RS485,
#endif
};

// ----------------------------------------------------------------------
// Programmer output pins:
//	LED	PB0	(D0)
//	VCC	PB1	(D1)
//	VCC	PB2	(D2)
//	VCC	PB3	(D3)
//	RESET	PB5	(D5)
//	SCK	PB6	(D6)
//	MOSI	PB7	(D7)
// ----------------------------------------------------------------------
#define	PORT		PORTB
#define	DDR		DDRB
#define DDRMASK 0xED
// Anpassung an atmega8
#define POWER_MASK      0x01
#define RESET_MASK      (1 << 2)
#define SCK_MASK        (1 << 5)
#define MOSI_MASK       (1 << 3)

// ----------------------------------------------------------------------
// Programmer input pins:
//	MISO	PD3	(ACK)
// ----------------------------------------------------------------------
#define	PIN		PINB
#define	MISO_MASK	(1 << 4)

// ----------------------------------------------------------------------
// Rs485 Pins
// ----------------------------------------------------------------------
#define RS485PORT PORTC
#define RS485_TX_RX     PC3

#define min(a,b) ((a) <(b)?(a):(b))

// ----------------------------------------------------------------------
// Local data
// ----------------------------------------------------------------------
static	byte_t		sck_period;	// SCK period in microseconds (1..250)
static	byte_t		poll1;		// first poll byte for write
static	byte_t		poll2;		// second poll byte for write
static	uint_t		address;	// read/write address
static	uint_t		timeout;	// write timeout in usec
static	byte_t		cmd0;		// current read/write command byte
static	byte_t		cmd[4];		// SPI command buffer
static	byte_t		res[4];		// SPI result buffer
static  byte_t          mode;           // current mode
#ifdef I2C_SUPPORT
static	byte_t		i2caddr;	// i2c function
static	byte_t		i2cstats[32]; // testbuf für i2c
static	byte_t		i2cstatspos; //ausgabezeiger für stat-abfrage
static	byte_t		i2cstat; //status der i2c kommunikation
static	byte_t		i2crecvlen; //anzahl der zu lesenden bytes
#endif
#ifdef RS485_SUPPORT
static	byte_t		rs485stat; //status der rs485 kommunikation
static  byte_t          rs485_read_pointer;
#endif

// ----------------------------------------------------------------------
// Delay exactly <sck_period> times 0.5 microseconds (6 cycles).
// ----------------------------------------------------------------------
__attribute__((always_inline))
static	void	delay ( void )
{
	asm volatile(
		"	mov	__tmp_reg__,%0	\n"
		"0:	rjmp	1f		\n"
		"1:	nop			\n"
		"	dec	__tmp_reg__	\n"
		"	brne	0b		\n"
		: : "r" (sck_period) );
}

// ----------------------------------------------------------------------
// Issue one SPI command.
// ----------------------------------------------------------------------
static	void	spi ( byte_t* cmd, byte_t* res )
{
	byte_t	i;
	byte_t	c;
	byte_t	r;
	byte_t	mask;

	for	( i = 0; i < 4; i++ )
	{
		c = *cmd++;
		r = 0;
		for	( mask = 0x80; mask; mask >>= 1 )
		{
			if	( c & mask )
			{
				PORT |= MOSI_MASK;
			}
			delay();
			PORT |= SCK_MASK;
			delay();
			r <<= 1;
			if	( PIN & MISO_MASK )
			{
				r++;
			}
			PORT &= ~ MOSI_MASK;
			PORT &= ~ SCK_MASK;
		}
		*res++ = r;
	}
}

// ----------------------------------------------------------------------
// USART Interupt Handler
// ----------------------------------------------------------------------
#ifdef RS485_SUPPORT
SIGNAL(SIG_UART_RECV)
{
  PORTD |= _BV(PD6);
  uint8_t c, tmp;
  // detect nine bit communication
  if (!(UCSRA & _BV(FE))) {
    // save ninth bit
    c = UCSRB & _BV(RXB8);
    tmp = UDR;
    fifo_push(tmp, (UCSRB & UCSZ2) && c);
  } else {
    tmp = UDR;
  }
  PORTD &= ~_BV(PD6);
}
#endif

// ----------------------------------------------------------------------
// Create and issue a read or write SPI command.
// ----------------------------------------------------------------------
static	void	spi_rw ( void )
{
	uint_t	a;

	a = address++;
	if	( cmd0 & 0x80 )
	{	// eeprom
		a <<= 1;
	}
	cmd[0] = cmd0;
	if	( a & 1 )
	{
		cmd[0] |= 0x08;
	}
	cmd[1] = a >> 9;
	cmd[2] = a >> 1;
	spi( cmd, res );
}

#ifdef I2C_SUPPORT
void i2c_wait_int()
{
	while( (TWCR & _BV(TWINT)) == 0);
}

byte_t i2c_send ( byte_t sendbyte )
{
	TWDR = sendbyte;
	TWCR |= _BV(TWINT);
	i2c_wait_int();
	return TWSR;
}
#endif

// ----------------------------------------------------------------------
// Handle a non-standard SETUP packet.
// ----------------------------------------------------------------------
extern	byte_t	usb_setup ( byte_t data[8] )
{
  byte_t	bit;
  byte_t	mask;
  byte_t*	addr;
  byte_t	req;

  // Generic requests
  req = data[1];
  if	( req == USBTINY_ECHO )
  {
    return 8;
  }
#ifdef RS485_SUPPORT
  // rs485
  if ( req == USBRS485_SETUP ) {
    rs485stat = RS485_SETUP;
    mode = MODE_RS485;
    return 0;
  }
  if ( req ==  USBRS485_START_TX ) {
    rs485stat = RS485_WRITE;
    // enable transmitter disable reciever
    RS485PORT |= _BV(RS485_TX_RX);
    mode = MODE_RS485;
    return 0;
  }
  if ( req == USBRS485_STOP_TX ) {
    rs485stat = 0;
    // enable reciever disable transmitter
    RS485PORT &= ~_BV(RS485_TX_RX);
    mode = MODE_RS485;
    return 0;
  }
  if ( req ==  USBRS485_WRITE ) {
    // set ninth bit
    if (data[4]) 
      UCSRB |= _BV(TXB8);
    else
      UCSRB &= ~_BV(TXB8);
    mode = MODE_RS485;
    return 0;
  }
  if ( req == USBRS485_READ_BUFFER_LEN ) {
    data[0] = buffer_count;
    mode = MODE_RS485;
    return 1;
  }
  if ( req == USBRS485_READ_BUFFER ) {
    rs485stat = RS485_READ;
    rs485_read_pointer = 0;
    mode = MODE_RS485;
    return 0xff;
  }
  if (req == USBRS485_CLEAR_BUFFER ) {
    if (!data[4]) 
      fifo_delete_lowest(buffer_count);
    else
      fifo_delete_lowest(data[4]);
    mode = MODE_RS485;
    return 0;
  }
#endif
#ifdef I2C_SUPPORT
  //i2c
  if	( req == USBI2C_STOP)
  {
    mode = MODE_I2C;
    if((i2cstat & (I2C_READ | I2C_WRITE)) != 0)
    {
      TWCR |= _BV(TWINT) | _BV(TWSTO);
      i2cstat = 0;
      PORTB &= ~_BV(PB1);
      return 0;
    }
    data[0] = i2cstat;
    return 1;
  }
  else if	( req == USBI2C_STAT)
  {
    i2cstat |= I2C_STAT | I2C_PAKET;
    i2cstatspos = 0;
    mode = MODE_I2C;
    return 0xff;
  }
  else if	( req == USBI2C_READ)
  {
    mode = MODE_I2C;
    i2cstats[0x00] = 0;
    i2cstats[0x04] = data[6];
    i2cstats[0x05] = data[7];
    if(data[7] == 0)
    {
      i2crecvlen = data[6];
    }
    else
    {
      i2crecvlen = 0;
    }
    i2cstat = (i2cstat | I2C_READ | I2C_PAKET) & ~I2C_STAT ;
    if(i2caddr != data[4] || (i2cstat & I2C_READ_ON) == 0)
    {
      i2caddr = data[4];
      i2cstat |= I2C_READ_ON;
      PORTB |= _BV(PB1);
      TWCR |= _BV(TWINT) | _BV(TWSTA);
      i2c_wait_int();
      i2cstats[0x01] = TWSR;
      byte_t TWSRtmp = 0;
      if(TWSR == 0x08 || TWSR == 0x10)
      {
        TWSRtmp = i2c_send ( i2caddr<<1 | 0x01 );
        TWCR &= ~_BV(TWSTA);
        i2cstats[0x02] = TWSRtmp;
      }
      else
      {
        TWCR = _BV(TWINT) | _BV(TWEN);
        data[0] = i2cstats[0x00] = TWSR;
        i2cstat &= ~(I2C_READ | I2C_PAKET | I2C_READ_ON);
        return 0;
      }
      if(TWSRtmp == 0x38 || TWSRtmp == 0x48)
      {
        TWCR = _BV(TWEN);
        data[0] = i2cstats[0x00] = TWSRtmp;
        i2cstat &= ~(I2C_READ | I2C_PAKET | I2C_READ_ON);
        return 0;
      }
      if(TWSRtmp != 0x40)
      {
        TWCR = _BV(TWEN);
        data[0] = i2cstats[0x00] = TWSRtmp;
        i2cstat &= ~(I2C_READ | I2C_PAKET | I2C_READ_ON);
        PORTB &= ~_BV(PB1);
        return 0;
      }
      if(data[6] > 1 || data[7] > 0)
        TWCR |= _BV(TWEA);

      TWCR |= _BV(TWINT);

    }
    return 0xff;
  }
  else if	( req == USBI2C_WRITE)
  {
    mode = MODE_I2C;
    i2cstats[0x00] = 0;
    i2cstat = I2C_PAKET;
    if(i2caddr != data[4] || (i2cstat & I2C_WRITE) == 0)
    {
      i2caddr = data[4];
      i2cstat |= I2C_WRITE;
      PORTB |= _BV(PB1);
      TWCR |= _BV(TWINT) | _BV(TWSTA);
      i2c_wait_int();
      i2cstats[0x01] = TWSR;
      byte_t TWSRtmp = 0;
      if(TWSR == 0x08 || TWSR == 0x10)
      {
        TWCR &= ~(_BV(TWSTA) | _BV(TWINT));
        TWSRtmp = i2c_send ( i2caddr<<1 & 0xFE );
        i2cstats[0x02] = TWSRtmp;
      }
      else
      {
        TWCR = _BV(TWINT) | _BV(TWEN);
        data[0] = i2cstats[0x00] = TWSR;
        i2cstat &= ~(I2C_WRITE | I2C_PAKET);
        return 0;

      }
      if(TWSRtmp == 0x20)
      {
        TWCR = _BV(TWINT) | _BV(TWEN);
        data[0] = i2cstats[0x00] = TWSRtmp;
        i2cstat &= ~(I2C_WRITE | I2C_PAKET);
        return 0;
      }
      if(TWSRtmp != 0x18)
      {
        TWCR = _BV(TWINT) | _BV(TWEN);
        data[0] = i2cstats[0x00] = TWSRtmp;
        i2cstat &= ~(I2C_WRITE | I2C_PAKET);
        PORTB &= ~_BV(PB1);
        return 0;
      }
    }
    return 0;
  }
  else
  {
    i2cstat &= ~I2C_PAKET;
  }
  //i2c ende
#endif
  addr = (byte_t*) (int) data[4];
  if	( req == USBTINY_READ )
  {
    data[0] = *addr;
    return 1;
  }
  if	( req == USBTINY_WRITE )
  {
    *addr = data[2];
    return 0;
  }
  bit = data[2] & 7;
  mask = 1 << bit;
  if	( req == USBTINY_CLR )
  {
    *addr &= ~ mask;
    return 0;
  }
  if	( req == USBTINY_SET )
  {
    *addr |= mask;
    return 0;
  }

  // Programming requests

  if	( req == USBTINY_POWERUP )
  {
    mode = MODE_SPI;
    sck_period = data[2];
    mask = POWER_MASK & DDRMASK;
    if	( data[4] )
    {
      mask |= RESET_MASK;
    }
    DDR  = DDRMASK;
    PORT = mask;
    return 0;
  }
  if	( req == USBTINY_POWERDOWN )
  {
    mode = MODE_SPI;
    PORT |= RESET_MASK;
    DDR  &= ~DDRMASK;
    DDRB |= _BV(PB1);
    return 0;
  }
  if	( ! PORT )
  {
    return 0;
  }
  if	( req == USBTINY_SPI ) {
    mode = MODE_SPI;
    spi( data + 2, data + 0 );
    return 4;
  }
  if	( req == USBTINY_POLL_BYTES )
  {
    mode = MODE_SPI;
    poll1 = data[2];
    poll2 = data[3];
    return 0;
  }
  address = * (uint_t*) & data[4];
  if	( req == USBTINY_FLASH_READ )
  {
    mode = MODE_SPI;
    cmd0 = 0x20;
    return 0xff;	// usb_in() will be called to get the data
  }
  if	( req == USBTINY_EEPROM_READ )
  {
    mode = MODE_SPI;
    cmd0 = 0xa0;
    return 0xff;	// usb_in() will be called to get the data
  }
  timeout = * (uint_t*) & data[2];
  if	( req == USBTINY_FLASH_WRITE )
  {
    mode = MODE_SPI;
    cmd0 = 0x40;
    return 0;	// data will be received by usb_out()
  }
  if	( req == USBTINY_EEPROM_WRITE )
  {
    mode = MODE_SPI;
    cmd0 = 0xc0;
    return 0;	// data will be received by usb_out()
  }
  return 0;
}

// ----------------------------------------------------------------------
// Handle an IN packet.
// ----------------------------------------------------------------------
extern	byte_t	usb_in ( byte_t* data, byte_t len )
{
	byte_t	i = 0;
#ifdef I2C_SUPPORT
        if (mode == MODE_I2C) {
          if((i2cstat & I2C_PAKET) != 0)
          {
            if((i2cstat & I2C_STAT) != 0){
              for	( i = 0; i < len; i++ )
              {
                data[i]=i2cstats[i2cstatspos++];
              }
            }
            else if ((i2cstat & I2C_READ) != 0){
              for	( i = 0; i < len; i++ )
              {
                i2c_wait_int();
                data[i] = TWDR;
                byte_t TWSRtmp = TWSR;
                i2cstats[0x08+i] = TWSRtmp;
                if(i2crecvlen > 0)
                  i2crecvlen--;
                if(i2crecvlen == 1)
                {
                  TWCR = (TWCR | _BV(TWINT)) & ~_BV(TWEA);
                }
                else
                {
                  TWCR |= _BV(TWINT);
                }
                if(TWSRtmp == 0x58)
                  i2cstat &= ~I2C_READ_ON;
                TWCR |= _BV(TWINT);
              }
            }
            return len;
          }
        }
#endif
#ifdef RS485_SUPPORT
        if (mode == MODE_RS485) {
          if (rs485stat == RS485_READ) {
            for (i = 0; i < len; i++)
              data[i] = data_buffer[rs485_read_pointer++];
            return len;
          }
        }
#endif
        if (mode == MODE_SPI) {
          for	( i = 0; i < len; i++ )
          {
            spi_rw();
            data[i] = res[3];
          }
        }
        return len;
}

// ----------------------------------------------------------------------
// Handle an OUT packet.
// ----------------------------------------------------------------------
extern	void	usb_out ( byte_t* data, byte_t len )
{
	byte_t	i;
	uint_t	usec;
	byte_t	r;
#ifdef I2C_SUPPORT
        if (mode == MODE_I2C) {
          if((i2cstat & I2C_PAKET) != 0x0)
          {
            for	( i = 0; i < len; i++ )
            {
              byte_t TWSRtmp = i2c_send ( data[i] );
              if(TWSRtmp != 0x28 && TWSRtmp != 0x30)
              {
                i2cstats[0x03] = i2cstats[0x00] = TWSRtmp;
                TWCR |= _BV(TWINT) | _BV(TWSTO);
                i2cstat = 0;
              }
            }
          }
        }
#endif
#ifdef RS485_SUPPORT
        if (mode == MODE_RS485) {
          if ( rs485stat == RS485_SETUP )
          {
            // bytes:
            // 2 baud rate/100
            // 3 stop bits ( first bit )
            // 4 parity ( first two bits )
            // 5 transmit size ( first three bits )

            // baud rate
            int tmp;
            // 12 mhz
            tmp = (120000 / 16) / data[0] - 1;
            UBRRH = (unsigned char) (tmp >> 8);
            UBRRL = (unsigned char) tmp;

            tmp = 0;


            // stop bits
            if (data[1] & 0x01)
              tmp |= _BV(USBS);

            // parity
            tmp |= (data[2] & 0x03) << UPM0;

            // format selection
            tmp |= _BV(URSEL) | (data[3] & 0x03) << UCSZ0;
            tmp |= data[3] & 0x04;

            UCSRC = tmp;
            UCSRB &= ~_BV(UCSZ2);
            if (data[3] & 0x04)
              UCSRB |= _BV(UCSZ2);

            rs485stat = 0;
            return;
          }
          else if ( rs485stat ==  RS485_WRITE ) {
            for (i = 0; i < len; i++) {
              while ( !(UCSRA & _BV(UDRE)));
              UDR =  data[i];
            }
            return;
          }
        }
#endif
        if (mode == MODE_SPI) {
          for	( i = 0; i < len; i++ )
          {
            cmd[3] = data[i];
            spi_rw();
            cmd[0] ^= 0x60;	// turn write into read
            for	( usec = 0; usec < timeout; usec += 32 * sck_period )
            {	// when timeout > 0, poll until byte is written
              spi( cmd, res );
              r = res[3];
              if	( r == cmd[3] && r != poll1 && r != poll2 )
              {
                break;
              }
            }
          }
        }
}

#ifdef I2C_SUPPORT
void i2c_init()
{
	DDRB |= _BV(PB1); // I2C led 
	TWCR = 0;
	TWSR &= ~(_BV(TWPS0) | _BV(TWPS1));//prescaler for twi
	TWBR = 10; //10;//max speed for twi, ca 333khz by 12Mhz Crystal
	TWCR |= _BV(TWEN); 
}
#endif

#ifdef RS485_SUPPORT
void
rs485_init()
{
  int tmp;
  DDRC |= _BV(RS485_TX_RX); // Enable tx/rx switch
  tmp = (F_CPU / 16) / 9600 - 1;

  UBRRH = (unsigned char) (tmp >> 8);
  UBRRL = (unsigned char) tmp;

  tmp = 0;

  UCSRB = (1 << TXEN);
  /* Set frame format: 8data */
  UCSRC = (1 << URSEL) | (3 << UCSZ0);
  UCSRB |= _BV(RXCIE);
  UCSRB |= _BV(RXEN);
}
#endif

// ----------------------------------------------------------------------
// Main
// ----------------------------------------------------------------------
__attribute__((naked))		// suppress redundant SP initialization
extern	int	main ( void )
{
	DDRD = (1<<0)|(1<<1)| (1<<6) | _BV(PD3) | _BV(PD7);
	PORTD |= _BV(PD3);
	wdt_enable(WDTO_2S);
	wdt_reset();
	
	usb_init();
#ifdef I2C_SUPPORT
	i2c_init();
#endif
#ifdef RS485_SUPPORT
        rs485_init();
#endif

	for	( ;; )
	{
		usb_poll();
		wdt_reset();
#ifdef I2C_SUPPORT		
		if(TWSR == 0x00){
			i2cstats[0x06]++;
			i2c_init();
		}
#endif
	}
	return 0;
}
