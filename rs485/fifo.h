#ifndef _AVR_FIFO_H
#define _AVR_FIFO_H

#include <stdlib.h>
#include <string.h>

#ifndef _AVR_FIFO_C
extern unsigned char data_buffer[255];
extern unsigned char commando_buffer[48];
extern unsigned char buffer_count;
extern unsigned char commando_count;
#endif

// Push byte into fifo
void fifo_push(unsigned char byte, unsigned char commando);
// delete lowest bytes from fifo
void fifo_delete_lowest(unsigned char count);

#endif
