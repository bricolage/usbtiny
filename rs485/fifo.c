#define _AVR_FIFO_C
#include "fifo.h"

volatile unsigned char data_buffer[255];
volatile unsigned char commando_buffer[48];
volatile unsigned char buffer_count = 0;
volatile unsigned char commando_count = 0;

void
fifo_delete_lowest(unsigned char count)
{
  // Interupts deaktivieren
  unsigned char c_move = 0;
  unsigned char i = 0;

  if (count > buffer_count) count = buffer_count;

  memmove((void *)data_buffer, (const char *)data_buffer + count, sizeof(data_buffer) - count);
  while ( i < commando_count) {
    if (commando_buffer[i] < count)
      c_move++;
    i++;
  }
  memmove((void *)commando_buffer, (const char *)commando_buffer + c_move, sizeof(commando_buffer) - c_move);
  commando_count -= c_move;
  buffer_count -= count;
  for (i = 0; i < commando_count; i ++) commando_buffer[i] -= count;
}

void 
fifo_push(unsigned char byte, unsigned char commando) 
{
  /* If there no place left in fifo, kill oldest data */
  if (buffer_count >= sizeof(data_buffer))
    fifo_delete_lowest(1);
  data_buffer[buffer_count++] = byte;
  if (commando) {
    if (commando_count < sizeof(commando_buffer))
      commando_buffer[commando_count++] = buffer_count - 1;
  }
}

