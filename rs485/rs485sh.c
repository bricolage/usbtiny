/**********************************************************
 * Copyright (C) 2007 by Christian Dietrich <stettberger@brokenpipe.de>, Germany
 * Copyright (C) 2006 by Jochen Roessner <jochen@lugrot.de>, Germany

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

/* Guile interface example:
 *
 * (define dev (rs485_open "19640001"))
 * (rs485_setup dev 9600 1 "none" 9)
 * (rs485_start_tx dev)
 * (rs485_write dev "Hallo" #t)
 * (rs485_stop_tx dev)
 *
 */
 
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <usb.h>
#include <guile/gh.h>

enum
{
     RS485_Stopbit_1,
     RS485_Stopbit_2,
};

enum
{
	// Generic requests
        USBRS485_SETUP = 30,
        USBRS485_START_TX,
        USBRS485_STOP_TX,
        USBRS485_WRITE,
        USBRS485_READ_BUFFER_LEN,        // Read the len of the recieve buffer
        USBRS485_READ_BUFFER,        // Read the the recieve buffer
        USBRS485_CLEAR_BUFFER,        // Read the the recieve buffer
};

struct rs485dev { 
	usb_dev_handle *usbdev;
        char *device;
};

static scm_t_bits scm_rs485dev_tag;

char *rs485_read_buffer(struct rs485dev *handle, unsigned char len);

enum RS485_Parity {
  RS485_Parity_None,
  RS485_Parity_Even,
  RS485_Parity_Odd,
};

struct rs485dev *
rs485_open(char *device) 
{
  struct rs485dev *handle;
  if((handle = malloc(sizeof(*handle))) == NULL){
    perror("malloc");
    return 0;
  }
  handle->device = strdup(device);
  struct usb_bus *bus;
  unsigned int idVendor;
  unsigned int idProduct;
  if (sscanf(device, "%04x%04x", &idVendor, &idProduct) == 2)
  {
    usb_init();
    usb_find_busses();
    usb_find_devices();
    for (bus = usb_busses; bus; bus = bus->next)
    {
      struct usb_device *dev;
      for (dev = bus->devices; dev; dev = dev->next)
      {
        usb_dev_handle *usb_bus_dev;
        usb_bus_dev = usb_open(dev);
        if (usb_bus_dev)
        {
          if (dev->descriptor.idVendor == idVendor &&
              dev->descriptor.idProduct == idProduct)
          {
            handle->usbdev = usb_bus_dev;
            return handle;
          }
        }
        if (usb_bus_dev)
          usb_close(usb_bus_dev);
      }
    }
  }
  free(handle->device);
  free(handle);
  return 0;
}

SCM
scm_rs485_open(SCM s_device)
{
  char *device;
  SCM smob;

  SCM_ASSERT(scm_is_string(s_device), s_device, SCM_ARG1, "rs485_open");
  device = scm_to_locale_string(s_device);

  struct rs485dev *handle = rs485_open(device);
  SCM_NEWSMOB(smob, scm_rs485dev_tag, handle);
  
  free(device);

  if (handle == 0)
    return SCM_BOOL_F;
  return smob;
}

int 
rs485_setup(struct rs485dev *handle, unsigned int baudrate, 
            unsigned char stopbit, unsigned char parity, 
            unsigned char framesize)
{
  baudrate /= 100;
  if (stopbit > RS485_Stopbit_2) stopbit = RS485_Stopbit_2;
  if (parity > RS485_Parity_Odd) parity = RS485_Parity_Odd;
  
  framesize -= 5;
  if (framesize > 3) framesize = 7;

  char data[4];
  data[0] = baudrate;
  data[1] = stopbit;
  data[2] = parity;
  data[3] = framesize;
  
  return usb_control_msg(handle->usbdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE 
                         | USB_ENDPOINT_OUT, USBRS485_SETUP, 0, 0, (char*) data, 4, 500);
}

SCM
scm_rs485_setup(SCM s_handle, SCM s_baudrate, SCM s_stopbit, SCM s_parity, SCM s_framesize)
{
  struct rs485dev *handle;
  char *parity_string;
  unsigned int baudrate;
  unsigned char stopbit, parity, framesize;

  scm_assert_smob_type(scm_rs485dev_tag, s_handle);
  SCM_ASSERT(scm_is_integer(s_baudrate), s_baudrate, SCM_ARG2, "rs485_setup");
  SCM_ASSERT(scm_is_integer(s_stopbit), s_stopbit, SCM_ARG3, "rs485_setup");
  SCM_ASSERT(scm_is_string(s_parity), s_parity, SCM_ARG4, "rs485_setup");
  SCM_ASSERT(scm_is_integer(s_framesize), s_framesize, SCM_ARG5, "rs485_setup");

  handle = (struct rs485dev *) SCM_SMOB_DATA (s_handle);
  baudrate = scm_to_uint(s_baudrate);
  framesize = scm_to_uchar(s_framesize);
  parity_string = scm_to_locale_string(s_parity);
  stopbit = scm_to_uchar(s_stopbit);

  SCM_ASSERT((framesize >= 5 && framesize <= 9), s_framesize, SCM_ARG5, "rs485_setup");
  SCM_ASSERT((stopbit >= 1 && stopbit <= 2), s_stopbit, SCM_ARG2, "rs485_setup");

  if (strcmp(parity_string, "odd") == 0)
    parity = RS485_Parity_Odd;
  else if (strcmp(parity_string, "even") == 0)
    parity = RS485_Parity_Even;
  else if (strcmp(parity_string, "none") == 0)
    parity = RS485_Parity_None;
  else {
    SCM_ASSERT(0, s_parity, SCM_ARG4, "rs485_setup");
  }

  free(parity_string);
  
  return scm_from_int(rs485_setup(handle, baudrate, stopbit, parity, framesize));
}


unsigned char
rs485_read_buffer_len(struct rs485dev *handle) {
  char buffer[1] = {0};
  int ret = usb_control_msg(handle->usbdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE |  USB_ENDPOINT_IN, USBRS485_READ_BUFFER_LEN,
                  0, 0, (char *)&buffer[0], 1, 500);
  return buffer[0];
}

SCM
scm_rs485_read_buffer_len(SCM s_handle)
{
  struct rs485dev *handle;
  scm_assert_smob_type(scm_rs485dev_tag, s_handle);
  handle = (struct rs485dev *) SCM_SMOB_DATA (s_handle);
  return scm_from_int(rs485_read_buffer_len(handle));
}

char *
rs485_read_buffer(struct rs485dev *handle, unsigned char len)
{
  char *buffer = malloc(len + 1);
  int ret = usb_control_msg(handle->usbdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE 
                            |  USB_ENDPOINT_IN, USBRS485_READ_BUFFER,
                            0, 0, (char *)buffer, len, 500);
  return buffer;
}

SCM
scm_rs485_read_buffer(SCM s_handle, SCM s_len)
{
  struct rs485dev *handle;
  scm_assert_smob_type(scm_rs485dev_tag, s_handle);
  SCM_ASSERT(scm_is_integer(s_len), s_len, SCM_ARG2, "rs485_clear_buffer");
  handle = (struct rs485dev *) SCM_SMOB_DATA (s_handle);
  return scm_take_locale_stringn(rs485_read_buffer(handle, scm_to_int(s_len)), 
                          scm_to_int(s_len));
}

int
rs485_clear_buffer(struct rs485dev *handle, unsigned char len)
{
  return usb_control_msg(handle->usbdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE 
                         | USB_ENDPOINT_IN, USBRS485_CLEAR_BUFFER, 0, len, 
                         0, 0, 500);
}

SCM
scm_rs485_clear_buffer(SCM s_handle, SCM s_len)
{
  struct rs485dev *handle;
  scm_assert_smob_type(scm_rs485dev_tag, s_handle);
  SCM_ASSERT(scm_is_integer(s_len), s_len, SCM_ARG2, "rs485_clear_buffer");
  handle = (struct rs485dev *) SCM_SMOB_DATA (s_handle);
  return scm_from_int(rs485_clear_buffer(handle, scm_to_int(s_len)));
}

int
rs485_start_tx(struct rs485dev *handle) 
{
  return usb_control_msg(handle->usbdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE 
                         | USB_ENDPOINT_IN, USBRS485_START_TX, 0, 0, 0, 0, 500);
}

SCM
scm_rs485_start_tx(SCM s_handle)
{
  struct rs485dev *handle;
  scm_assert_smob_type(scm_rs485dev_tag, s_handle);
  handle = (struct rs485dev *) SCM_SMOB_DATA (s_handle);
  return scm_from_int(rs485_start_tx(handle));
}

int
rs485_stop_tx(struct rs485dev *handle) 
{
  return  usb_control_msg(handle->usbdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE 
                          | USB_ENDPOINT_IN, USBRS485_STOP_TX, 0, 0, 0, 0, 500);
}

SCM
scm_rs485_stop_tx(SCM s_handle)
{
  struct rs485dev *handle;
  SCM ret;

  scm_assert_smob_type(scm_rs485dev_tag, s_handle);
  handle = (struct rs485dev *) SCM_SMOB_DATA (s_handle);
  return scm_from_int(rs485_stop_tx(handle));
}


int 
rs485_write(struct rs485dev *handle, char *data, int len, char ninth_bit)
{
  return  usb_control_msg(handle->usbdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE 
                          | USB_ENDPOINT_OUT, USBRS485_WRITE, 0, ninth_bit?1:0,
                          data, len, 500);
}

SCM
scm_rs485_write(SCM s_handle, SCM s_data, SCM s_ninth_bit)
{
  struct rs485dev *handle;
  char *data;
  size_t len;
  SCM ret;


  scm_assert_smob_type(scm_rs485dev_tag, s_handle);
  SCM_ASSERT(scm_is_string(s_data), s_data, SCM_ARG2, "rs485_write");
  SCM_ASSERT(scm_is_bool(s_ninth_bit), s_ninth_bit, SCM_ARG3, "rs485_write");

  handle = (struct rs485dev *) SCM_SMOB_DATA (s_handle);
  data = scm_to_locale_stringn(s_data, &len);
  ret = scm_from_int(rs485_write(handle, data, len, scm_is_true(s_ninth_bit)));  
  free(data);

  return ret;
} 

int
rs485_close(struct rs485dev *handle)
{
	if(handle != NULL)
	{
                rs485_stop_tx(handle);
		usb_close(handle->usbdev);
                free(handle->device);
                free(handle);
		return 1;
	}
	return -1;
}

SCM
scm_rs485_close(SCM s_handle)
{
  struct rs485dev *handle;
  scm_assert_smob_type(scm_rs485dev_tag, s_handle);
  handle = (struct rs485dev *) SCM_SMOB_DATA (s_handle);
  rs485_close(handle);
  return SCM_UNSPECIFIED;
} 
 
static int
scm_rs485dev_print(SCM smob, SCM port, scm_print_state *pstate)
{
  struct rs485dev *handle = (struct rs485dev *) SCM_SMOB_DATA (smob);

  scm_puts("#<rs485dev ", port);
  scm_display(scm_from_locale_string(handle->device), port);
  scm_puts(">", port);

  /* non-zero means success */
  return 1;
}

void 
register_procs()
{
  gh_new_procedure("rs485_open", scm_rs485_open,  1, 0, 0);
  gh_new_procedure("rs485_close", scm_rs485_close,  1, 0, 0);
  gh_new_procedure("rs485_write", scm_rs485_write,  3, 0, 0);
  gh_new_procedure("rs485_start_tx", scm_rs485_start_tx,  1, 0, 0);
  gh_new_procedure("rs485_stop_tx", scm_rs485_stop_tx,  1, 0, 0);
  gh_new_procedure("rs485_setup", scm_rs485_setup,  5, 0, 0);
  gh_new_procedure("rs485_read_buffer_len", scm_rs485_read_buffer_len,  1, 0, 0);
  gh_new_procedure("rs485_clear_buffer", scm_rs485_clear_buffer,  2, 0, 0);
  gh_new_procedure("rs485_read_buffer", scm_rs485_read_buffer,  2, 0, 0);

  scm_rs485dev_tag = scm_make_smob_type("rs485dev", sizeof (struct rs485dev));
  scm_set_smob_print(scm_rs485dev_tag, scm_rs485dev_print);
}

void
inner_main(int argc, char **argv)
{
  register_procs();
  gh_repl(argc, argv);
}

int 
main(int argc, char **argv)
{
  gh_enter(argc, argv, inner_main);
  return(0); /* never reached */
}

