(use-modules (ice-9 format))
(use-modules (ice-9 readline))
(use-modules (ice-9 regex))

(display "RS485 Shell (type w<enter> to get an input line)\n")

(define print-as-hex (lambda (s) 
                      (begin
                       (for-each 
                        (lambda (x) 
                         (format #t "0x~:@(~2,'02x ~)" (char->integer x))) 
                        (string->list s))
                       (newline))))
(define send-str (lambda (dev x) 
                  (let ((regex "\\s*[0\\]x([0-9a-fA-F]+)\\s*")) 
                    (begin 
                     (display "Send: ")
                     (send-blob dev (regexp-substitute/global #f regex x 
                               'pre 
                               (lambda (m) (integer->char 
                                            (string->number  
                                            (match:substring m 1) 16)))
                               'post))))))
(define send-blob (lambda (dev x) 
                   (begin 
                    (print-as-hex x)
                    (rs485_start_tx dev)
                    (rs485_write dev x #f)
                    (rs485_stop_tx dev))))


(define dev (rs485_open "19640001"))
(rs485_setup dev 9600 1 "none" 8)
(while #t (let ((len (rs485_read_buffer_len dev))) 
           (begin
            (if (char-ready? (current-input-port))
             (if (eq? (read-char (current-input-port)) #\w)
              (begin 
               (while (char-ready? (current-input-port)) (read-char
                                                          (current-input-port)))
               (send-str dev (readline "Send: ")))))
            (if (not (eq? len 0)) 
             (begin 
              (display "Recv: ")
              (print-as-hex (rs485_read_buffer dev len))
              (rs485_clear_buffer dev len)))
            (usleep 100000))))
