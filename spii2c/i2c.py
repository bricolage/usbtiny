#!/usr/bin/python
# ======================================================================
# spi.py - USBtiny/spi test program
# ======================================================================

import sys, os.path
sys.path[0] = os.path.join(sys.path[0], '../util')
import usbtiny

vendor	= 0x1964
product	= 0x0001

# Generic requests
USBTINY_ECHO		= 0	# echo test
USBTINY_READ		= 1	# read byte (wIndex:address)
USBTINY_WRITE		= 2	# write byte (wIndex:address, wValue:value)
USBTINY_CLR		= 3	# clear bit (wIndex:address, wValue:bitno)
USBTINY_SET		= 4	# set bit (wIndex:address, wValue:bitno)
# Programming requests
USBTINY_POWERUP		= 5	# apply power (wValue:SCK-period, wIndex:RESET)
USBTINY_POWERDOWN	= 6	# remove power from chip
USBTINY_SPI		= 7	# issue SPI command (wValue:c1c0, wIndex:c3c2)
USBTINY_POLL_BYTES	= 8	# set poll bytes for write (wValue:p1p2)
USBTINY_FLASH_READ	= 9	# read flash (wIndex:address)
USBTINY_FLASH_WRITE	= 10	# write flash (wIndex:address, wValue:timeout)
USBTINY_EEPROM_READ	= 11	# read eeprom (wIndex:address)
USBTINY_EEPROM_WRITE	= 12	# write eeprom (wIndex:address, wValue:timeout)
USBI2C_READ = 20 #read from i2c bus
USBI2C_WRITE = 21 #write to i2c bus
USBI2C_STOP	=	22 #send stop condition
USBI2C_STAT = 23 #get stats from i2c action

usage = """Available commands:
	t                          - echo test
	U <reset>                  - powerup; set RESET to <reset> (1 = off)
	D                          - powerdown
	r <addr> <count>           - read bytes from i2c <addr>
	w <addr> <ascii data>      - write bytes to i2c <addr>
	s                          - send Stop Condition to i2c
	S                          - read stats from i2c action"""
	
"""	
	c <addr> <bit>             - clear bit at <addr>
	s <addr> <bit>             - set bit at <addr>
	U <reset>                  - powerup; set RESET to <reset>
	D                          - powerdown
	C <c1> <c2> <c3> <c4>      - issue SPI command
	P <p1> <p2>                - set poll bytes
	f <addr> <count>           - read from flash
	e <addr> <count>           - read from EEPROM
	F <addr> <timeout> <byte>  - write byte to flash
	E <addr> <timeout> <byte>  - write byte to EEPROM
	w <addr> <byte>            - write byte to i2c bus"""

def i2cdump(data):
	if not data:
		return
	print "len: %i" % len(data)
	for i in range(len(data)):
		if (i % 16) == 0:
			if i > 0:
				print
		print "%02x" % ord(data[i]),
	print

# --- check arguments
cmd  = None
i2c  = None
addr = 0x00
byte = 0x00
c3   = 0x00
c4   = 0x00
data = ""

argc = len(sys.argv)
if argc > 1:
	cmd = sys.argv[1][0]
if argc > 2 and cmd != 't':
	byte = int(sys.argv[2], 16)
if argc > 3:
	addr = byte
	if cmd == 'w':
		buf = sys.argv[3] #chr(byte)
	else:
		byte = int(sys.argv[3], 16)
if argc > 4:
	c3 = int(sys.argv[4], 16)
if argc > 5:
	c4 = int(sys.argv[5], 16)
if  not (argc == 2 and cmd in "tsD")\
and not (argc == 3 and cmd in "US")\
and not (argc == 4 and cmd in "wr"):
	print >> sys.stderr, usage
	sys.exit(1)
	
# --- open USB device
dev = usbtiny.USBtiny(vendor, product)

# --- dispatch command
if cmd == 't':
	dev.echo_test()
elif cmd == 's':
	r = dev.control_in(USBI2C_STOP, 0, 0, 0)
	i2cdump(r)
elif cmd == 'S':
	r = dev.control_in(USBI2C_STAT, 0, 0, byte)
	if not r:
		print >> sys.stderr, "error: no response"
		sys.exit(1)
	i2cdump(r)
elif cmd == 'w':
	r = dev.control_out(USBI2C_WRITE, 0, addr, buf)
	if not r or r < 1:
		print >> sys.stderr, "error: %i" % r
		sys.exit(1)
elif cmd == 'r':
	while True:
		r = dev.control_in(USBI2C_READ, 0, addr, byte)
		if not r:
			print >> sys.stderr, "error: no response"
			sys.exit(1)
		data = data + r
		if byte > 0xff:
			byte = byte - 0xff
		else:
			break
	i2cdump(data)
elif cmd == 'U':
	dev.control_in(USBTINY_POWERUP, 20, byte, 0)
elif cmd == 'D':
	dev.control_in(USBTINY_POWERDOWN, 0, 0, 0)


'''
elif cmd == 'r':
	addr = byte
	byte = dev.control_in(USBTINY_READ, 0, addr, 1)
	print "%02x: %02x" % (addr, ord(byte))
elif cmd == 'w':
	dev.control_in(USBTINY_WRITE, byte, addr, 0)
elif cmd == 'c':
	dev.control_in(USBTINY_CLR, byte, addr, 0)
elif cmd == 's':
	dev.control_in(USBTINY_SET, byte, addr, 0)
elif cmd == 'U':
	dev.control_in(USBTINY_POWERUP, 20, byte, 0)
elif cmd == 'D':
	dev.control_in(USBTINY_POWERDOWN, 0, 0, 0)
elif cmd == 'C':
	r = dev.control_in(USBTINY_SPI, addr + (byte << 8), c3 + (c4 << 8), 4)
	if len(r) == 4:
		print "%02x %02x %02x %02x" % tuple([ord(i) for i in r])
	else:
		print "No power"
elif cmd == 'P':
	dev.control_in(USBTINY_POLL_BYTES, addr + (byte << 8), 0, 0)
elif cmd == 'f':
	r = dev.control_in(USBTINY_FLASH_READ, 0, addr, byte)
	usbtiny.dump(addr, r)
elif cmd == 'e':
	r = dev.control_in(USBTINY_EEPROM_READ, 0, addr, byte)
	usbtiny.dump(addr, r)
elif cmd == 'F':
	buf = chr(c3)
	dev.control_out(USBTINY_FLASH_WRITE, byte, addr, buf)
elif cmd == 'E':
	buf = chr(c3)
	dev.control_out(USBTINY_EEPROM_WRITE, byte, addr, buf)
'''

